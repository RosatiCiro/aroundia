Aroundia is an app for people who have few time to visit a place. It is an app suitable for those who stand still waiting for a means of transport and want to use the time / hours they have available to discover the area around. It is an app for people who are in a new city, for work or study reasons, and have the desire to explore its surroundings.

Aroundia is composed of three sections: Discover Now, Plan Your Next Itinerary and Places Discovered.

Discover now is a section that fast generate itineraries based on the time available and your personal interests. In particular, after selecting the time, the interests and the final destination, the app automatically generates an itinerary trough the current position. The itinerary will be displayed on a map and you have the possibility to remove and add other steps, choosing among those nearby, highlighted on the map, or by using the search bar above.

In the event that, by adding stages to the itinerary, you go beyond the time available initially indicated, there will be a warning through a signal explaining that the time available is not enough. In any case, you have the choice to continue.

The itinerary is generated according to the time to reach the attractions plus the time the user usually spends on them. The directions will be given via Google Maps. You have the option to stop and recalculate the itinerary any time and, once finished, you can save the itinerary in the “Places Discovered” section and revise it whenever he wants.

The Plan Your Next Itinerary section allows you to plan future itineraries. This section works at the same way as the first with the difference that the user can also chose the city and the date.

Once planned, the saved itinerary will be shown in the “Planned Itineraries” subsection where you can revise it whenever you want and start it when you needs to.

In Places Discovered section all the places seen and the completed itineraries are stored.

For what concerns the places, they are organized by city and you have the possibility to add the cities or places you had already visited, so that those will not appear in the creation of future itineraries. In the section regarding the carried out itineraries, the app displays them and gives you the possibility to rename and review them.

https://apps.apple.com/it/app/aroundia/id1513816163


During this challenge, I worked as a Back-End and a bit of Front-End developer. I did some functionalities regarding the use of Google APIs and mainly worked on the first and third section of the app. 